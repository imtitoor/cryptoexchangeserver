const express = require('express');
const cors = require('cors');
const connection = require('./src/config/db');
const app = express();
const PORT = process.env.PORT ?? 3001;
const users = require('./src/routes/users');
const wallets = require('./src/routes/wallet');
const accounts = require('./src/routes/accounts');
const payments = require('./src/routes/payments');
const market = require('./src/routes/market');

app.use(express.json());
app.use(cors())

connection.connect(err => {
    if (err)
        console.log(err)
});

app.use('/users', users);
app.use('/wallets', wallets);
app.use('/accounts', accounts);
app.use('/payments', payments);
app.use('/market', market);

app.get('/', (req, res) => {
    res.status(200).send('Welcome to crypt exchange api');
})

app.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`)
})