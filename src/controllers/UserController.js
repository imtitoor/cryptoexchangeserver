const UsersModal = require("../models/UsersModel");

function getAll(req, res) {
    return new UsersModal(req, res).getAllUsers()
}

function createUser(req, res) {
    return new UsersModal(req, res).createUser();
}

function findUser(req, res) {
    return new UsersModal(req, res).findUser();
}

const UserController = {
    getAll,
    createUser,
    findUser
}

module.exports = UserController;