const MarketsModel = require("../models/MarketsModel");

function searchAsset(req, res) {
    return new MarketsModel(req, res).searchAssets();
}

function getTopAssets(req, res) {
    return new MarketsModel(req, res).getTopAssets();
}

function getExchangeData(req, res) {
    return new MarketsModel(req, res).getExchangeData();
}

function getOrderBook(req, res) {
    return new MarketsModel(req, res).getOrderBookData();
}

const MarketsController = {
    searchAsset,
    getTopAssets,
    getExchangeData,
    getOrderBook
}

module.exports = MarketsController;