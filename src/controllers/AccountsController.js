const AccountsModel = require("../models/AccountsModel");

function getAccountDetails(req, res) {
    return new AccountsModel(req, res).getAccountDetails();
}

const AccountsController = {
    getAccountDetails
}

module.exports = AccountsController;