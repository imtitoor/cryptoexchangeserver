const PaymentsModel = require("../models/PaymentsModel");

function getPayments(req, res) {
    return new PaymentsModel(req, res).getAccountPayments();
}

function createPayment(req,res){
    return new PaymentsModel(req,res).createPayment();
}

const PaymentsController = {
    getPayments,
    createPayment,
}

module.exports = PaymentsController;