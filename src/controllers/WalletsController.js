const WalletsModel = require("../models/WalletsModel");

function getAllWallets(req, res) {
    return new WalletsModel(req, res).getAllWallets();
}

function getAllUserWallets(req, res) {
    return new WalletsModel(req, res).getUserWallets();

}

function findById(req, res) {
    return new WalletsModel(req, res).getWalletById();
}

function createWallet(req, res) {
    return new WalletsModel(req, res).createWallet();
}

function activateWallet(req, res) {
    return new WalletsModel(req, res).activateWallet();
}


const WalletsController = {
    getAllWallets,
    getAllUserWallets,
    findById,
    createWallet,
    activateWallet
}

module.exports = WalletsController;