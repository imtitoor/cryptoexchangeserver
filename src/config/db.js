const db = require('mysql');

const connection = db.createConnection({
    host: 'localhost',
    insecureAuth: true,
    user: 'root',
    password: '',
    database: 'cryptoexchange'
})

module.exports = connection;