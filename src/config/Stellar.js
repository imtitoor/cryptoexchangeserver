const StellarSDK = require('stellar-sdk');
const TEST_URL = "https://horizon-testnet.stellar.org";
const LIVE_URL = "https://horizon.stellar.org"
const stellarServer = new StellarSDK.Server(TEST_URL);
const stellarLiveServer = new StellarSDK.Server(LIVE_URL);

const Stellar = {
    SDK: StellarSDK,
    server: stellarServer,
    StellarSDK,
    stellarServer,
    stellarLiveServer
}

module.exports = Stellar;