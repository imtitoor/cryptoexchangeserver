const { server } = require('../config/Stellar');

class AccountsModel {
    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    async getAccountDetails() {
        const { public_key } = this.req.params;

        if (public_key) {
            try {
                const response = await server.loadAccount(public_key);

                if (response)
                    return this.res.status(200).send(response);

            } catch (error) {
                if (error.response.status == 404) {
                    return this.res.status(200).send({ message: 'Please activate your wallet to continue' });
                }
                return this.res.status(500).send(error);
            }
        }

        return this.res.status(400).send(new Error('Wallet key is required!'));
    }

}
module.exports = AccountsModel;