const { default: axios } = require("axios");
const moment = require("moment");
const connection = require("../config/db");
const { StellarSDK } = require("../config/Stellar");

class WalletsModel {

    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    getAllWallets() {
        connection.query('select * from wallets', (err, results) => {
            if (err)
                return this.res.status(500).send(new Error(err))
            return this.res.status(200).send(results);
        })
    }

    getWalletById() {
        let { id } = this.req.params;

        if (id)
            return connection.query(`select * from wallets where id = ${id}`, (err, results) => {
                if (err)
                    return this.res.status(500).send(new Error(err));

                return this.res.status(200).send({ results, id: id });
            });

        return this.res.status(400).send('User Id is required');
    }

    getUserWallets() {
        const { userId } = this.req.params;

        if (userId)
            return connection.query(`Select * from wallets where user_id=${userId}`,
                (err, results) => {
                    if (err)
                        return this.res.status(500).send(new Error(err));

                    if (results.length == 0)
                        return this.res.status(200).send({ message: 'No Wallets found' });

                    return this.res.status(200).send({ data: results })
                })
        return this.res.status(400).send('User Id is required');
    }

    createWallet() {
        const { id } = this.req.body;

        if (id) {
            let pair = StellarSDK.Keypair.random();
            let data = {
                secretKey: pair.secret(),
                publicKey: pair.publicKey()
            }

            return connection.query(`insert into wallets (user_id,public_key,secrety_key) values (${id},"${data.publicKey}","${data.secretKey}")`,
                (err, results) => {
                    if (err) {
                        console.log(err)
                        return this.res.status(500).send(new Error(err));
                    }

                    if (results)
                        return this.res.status(201).send({
                            message: 'Wallet Created Successfully. Please add lumens to activate account. Please save secret key to retreive wallet.',
                            data
                        })
                })
        }

        return this.res.status(400).send(new Error('User id is required'));
    }

    async activateWallet() {
        const { public_key } = this.req.body;

        if (public_key) {
            try {
                const response = await axios.get(`https://friendbot.stellar.org?addr=${encodeURIComponent(
                    public_key,
                )}`)

                if (response.status == 200) {
                    return connection.query(`Update wallets set active_at = "${moment(response.data.created_at).format('YYYY-MM-DD hh:mm:ss')}" where public_key ="${public_key}" `,
                        (err, results) => {
                            if (err)
                                return this.res.status(500).send(err);
                            if (results)
                                return this.res.status(200).send({ data: response.data, message: 'SUCCESS! You have activated your account' })
                        })
                }

            } catch (e) {
                return this.res.status(500).send(e);
            }
        }

        return this.res.status(400).send(new Error('Wallet key is required'));
    }

}

module.exports = WalletsModel;