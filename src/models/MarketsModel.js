const { default: axios } = require("axios");
const { Asset } = require("stellar-sdk");
const { stellarLiveServer } = require("../config/Stellar");
const moment = require('moment');

class MarketsModel {
    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    async searchAssets() {
        const { query } = this.req.params;
        const response1 = stellarLiveServer.assets().forCode(query).limit(200).call();
        const response2 = stellarLiveServer.assets().forIssuer(query).limit(200).call();

        Promise.allSettled([response1, response2])
            .then(val => {
                let rejectCount = 0;

                val.map((item) => {
                    item.status == 'rejected' ? rejectCount++ : null
                })

                if (rejectCount === val.length)
                    return this.res.status(400).send(new Error('Invalid Request'));

                const data = val
                    .map((item) => {
                        if (item.status == 'fulfilled')
                            return item.value.records.sort((a, b) => {
                                if (a.num_accounts < b.num_accounts)
                                    return 1;
                                if (a.num_accounts > b.num_accounts)
                                    return -1;
                                return 0;
                            })
                    });



                return this.res.status(200).send(data.filter((record) => record !== undefined))
            })
            .catch(error => {
                const { response } = error;
                return this.res.status(response.status).send(new Error(response.extras.reason))
            })
    }

    async getTopAssets() {
        // let yahooFinanceKey = 'kt77eLfKH8SbQCbznGDW99WZAJh8NbTaImWAmuS2';
        let yahooFinanceKey = 'YI57LDFET2312SbBjRTv2a5zbVoFdFLl2hpfZ7UH';
        let topAssets = [
            {
                assetCode: 'XLM',
                assetType: 'native',
                issuer: 'native',
                domain: 'Stellar Network',
                price: 0,
            },
            {
                assetCode: 'yXLM',
                assetType: 'credit_alphanum4',
                issuer: 'GARDNV3Q7YGT4AKSDF25LT32YSCCW4EV22Y2TV3I2PU2MMXJTEDL5T55',
                domain: 'https://ultrastellar.com',
                price: 0,
            },
            {
                assetCode: 'USDC',
                assetType: 'credit_alphanum4',
                issuer: 'GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN',
                domain: 'https://centre.io',
                price: 0,
            },
            {
                assetCode: 'XRP',
                assetType: 'credit_alphanum4',
                issuer: 'GBXRPL45NPHCVMFFAYZVUVFFVKSIZ362ZXFP7I2ETNQ3QKZMFLPRDTD5',
                domain: 'https://fchain.io',
                price: 0,

            },
            {
                assetCode: 'ETH',
                assetType: 'credit_alphanum4',
                issuer: 'GBFXOHVAS43OIWNIO7XLRJAHT3BICFEIKOJLZVXNT572MISM4CMGSOCC',
                domain: 'https://ultrastellar.com',
                price: 0,

            },
            {
                assetCode: 'BTC',
                assetType: 'credit_alphanum4',
                issuer: 'GDPJALI4AZKUU2W426U5WKMAT6CN3AJRPIIRYR2YM54TL2GDWO5O2MZM',
                domain: 'https://ultrastellar.com',
                price: 0,

            },
            {
                assetCode: 'BCH',
                assetType: 'credit_alphanum4',
                issuer: 'GBCHK4TINAK6KXTD4BBWWKR5BI2OKJ7C7HYX55BJ5I4KHNQMTL5UXBCH',
                domain: 'https://fchain.io',
                price: 0,

            },
        ]


        function getDatafromStellar() {
            let records;
            return Promise
                .allSettled(topAssets
                    .map(async (asset, index) => index !== 0 ? await stellarLiveServer.orderbook(new Asset(asset.assetCode, asset.issuer), new Asset.native()).limit(1).call() : null))
                .then(values => {
                    return records = values.map(record => record.value)
                })
                .catch(err => console.log(err));

        }

        axios
            .get('https://yfapi.net/v6/finance/quote?symbols=XLM-USD', { headers: { 'x-api-key': yahooFinanceKey } })
            .then(async (val) => {
                const { status, data } = val;

                if (status == 200) {
                    let price = data.quoteResponse.result[0].regularMarketPrice;
                    topAssets[0].price = price;

                    let records = await getDatafromStellar();
                    return this.res.status(200).send({ records, priceOfXLM: price });
                }


                let records = await getDatafromStellar();
                return this.res.status(200).send({ records, priceOfXLM: price });

            })
            .catch(err => {
                return this.res.status(err.response.status ?? 500).send(new Error(err.message ? err?.message : err?.data?.message ? err.data.message : err.response.data.message))
            });

    }

    async getExchangeData() {
        const { asset1, asset2 } = this.req.body;
        const res = this.res;

        if (asset1 == '' || asset2 == '')
            return this.res.status(400).send(new Error('Missing required data'));

        async function RetrieveData() {
            // Multiplied by 1000 to get Epoch time
            let prev2Day = moment().subtract(2, 'days').unix() * 1000;
            let currentDay = moment().unix() * 1000

            try {
                const data = await stellarLiveServer
                    .tradeAggregation(new Asset(asset1.code, asset1.issuer),
                        new Asset(asset2.code, asset2.issuer), prev2Day, currentDay, 86400000, 0)
                    .call()

                return res.status(200).send(data);

            } catch (error) {
                const { response } = error;

                if (response)
                    return res.status(response?.status ?? 500).send(response.title)

                return res.status(500);
            }

        }

        return RetrieveData();
    }

    async getOrderBookData() {
        const { asset1, asset2 } = this.req.body;
        const res = this.res;

        if (asset1 == '' || asset2 == '')
            return this.res.status(400).send(new Error(
                'Required data is missing'
            ))

        async function getStellarData() {
            try {
                const data = await stellarLiveServer
                    .orderbook(new Asset(asset1.code, asset1.issuer), new Asset(asset2.code, asset2.issuer))
                    .call()
                return res.status(200).send(data);
            } catch (error) {
                return res.status(500).send(error)
            }
        }
        return getStellarData();
    }
}

module.exports = MarketsModel;