const { stellarServer, StellarSDK } = require("../config/Stellar");


class PaymentsModel {

    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    getAccountPayments() {
        const { accountId } = this.req.params;
        if (accountId)
            return stellarServer.payments()
                .forAccount(accountId)
                .call()
                .then(data => {
                    return this.res.status(200).send(data)
                })
                .catch(error => {
                    return this.res.status(500).send(error)
                });

        return this.res.status(400).send(new Error('Account ID is required!'));
    }

    createPayment() {
        const { senderKey, destinationId, amount, asset } = this.req.body;

        if (senderKey == '' || destinationId == '' || amount == '' || asset == '')
            return this.res.status(400).send(new Error('Missing required fields'));

        var sourceKeys = StellarSDK.Keypair.fromSecret(
            senderKey,
        );
        let transaction;
        const response = this.res;
        
        stellarServer
            .loadAccount(destinationId)
            .catch(function (error) {
                if (error instanceof StellarSDK.NotFoundError) {
                    throw new Error("The destination account does not exist!");
                } else return error;
            })
            .then(function () {
                return stellarServer.loadAccount(sourceKeys.publicKey());
            })
            .then(function (sourceAccount) {
                // Start building the transaction.
                transaction = new StellarSDK.TransactionBuilder(sourceAccount, {
                    fee: StellarSDK.BASE_FEE,
                    networkPassphrase: StellarSDK.Networks.TESTNET,
                })
                    .addOperation(
                        StellarSDK.Operation.payment({
                            destination: destinationId,
                            asset: StellarSDK.Asset.native(),
                            amount: amount,
                        }),
                    )
                    .addMemo(StellarSDK.Memo.text("Testing Transaction"))
                    // Wait a maximum of three minutes for the transaction
                    .setTimeout(180)
                    .build();
                // Sign the transaction to prove you are actually the person sending it.
                transaction.sign(sourceKeys);
                // And finally, send it off to Stellar!
                return stellarServer.submitTransaction(transaction);
            })
            .then(function (result) {
                return response.status(200).send({ data: result, message: 'Payment Successfull' })
            })
            .catch(function (error) {
                console.error("Something went wrong!", error);
                return response.status(500).send(error)

                // If the result is unknown (no response body, timeout etc.) we simply resubmit
                // already built transaction:
                // stellarServer.submitTransaction(transaction);
            });
    }
}

module.exports = PaymentsModel;