const connection = require("../config/db");

class UsersModal {

    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    getAllUsers() {
        connection.query('select * from user', (err, results) => {
            if (err)
                return this.res.status(500).send(new Error(err));

            return this.res.status(200).send(results);
        })
    }

    findUser() {
        const { email, password } = this.req.params;

        if (email == '' || password == '')
            return this.res.status(400).send('Missing required fields');

        return connection.query(`select id,name,email from user where email="${email}" AND password="${password}"`,
            (error, results) => {
                if (error)
                    return this.res.status(500).send(error);

                if (results.length == 0) {
                    return this.res.status(200).send({ message: 'No User found' })
                }

                return this.res.status(200).send({ data: results[0] });
            })
    }

    alreadyExists() {
        const { email } = this.req.body;

        return new Promise((resolve, reject) => {
            connection.query(`select * from user where email = "${email}"`, (error, results) => {
                if (error) {
                    reject(new Error(error))
                }
                if (results.length > 0) {
                    resolve(true);
                }

                resolve(false);
            });
        })

    }
    
    createUser() {
        const { email, name, phone, dob, password } = this.req.body;

        if (email == '' || phone == '' || dob == '' || password == '' || name == '')
            return this.res.status(400).send('Required Fields are missing');


        return this.alreadyExists().then((val) => {
            if (val)
                return this.res.status(409).send(new Error('User already exists'));

            return connection.query(`insert into user (name,phone,dob,email,password) Values ("${name}","${phone}","${dob}","${email}","${password}")`,
                (error, results) => {
                    if (error)
                        return this.res.status(500).send(new Error(error));

                    if (results)
                        return this.res.status(201).send('User created successfully')
                });

        })

    }
}

module.exports = UsersModal;