const express = require('express');
const MarketsController = require('../controllers/MarketsController');
const router = express.Router();

router.get('/', MarketsController.getTopAssets);
router.get('/:query', MarketsController.searchAsset);
router.post('/exchange',MarketsController.getExchangeData);
router.post('/orderbook',MarketsController.getOrderBook);

module.exports = router;