const express = require('express');
const AccountsController = require('../controllers/AccountsController');
const router = express.Router();

router.get('/:public_key', AccountsController.getAccountDetails);

module.exports = router;