const express = require('express');
const UserController = require('../controllers/UserController');

const router = express.Router();

router.get('/', UserController.getAll);
router.get('/:email/:password',UserController.findUser);
router.post('/', UserController.createUser);

module.exports = router;