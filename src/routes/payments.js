const express = require('express');
const PaymentsController = require('../controllers/PaymentsController');
const router = express.Router();

router.get('/:accountId', PaymentsController.getPayments);

router.post('/', PaymentsController.createPayment);

module.exports = router;