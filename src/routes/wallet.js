const express = require('express');
const WalletsController = require('../controllers/WalletsController');
const router = express.Router();

router.get('/userWallets/:userId', WalletsController.getAllUserWallets);
router.get('/', WalletsController.getAllWallets)
router.get('/:id', WalletsController.findById);
router.post('/', WalletsController.createWallet)
router.post('/activate', WalletsController.activateWallet);

module.exports = router;